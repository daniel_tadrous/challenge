//
//  UIColorExtension.swift
//  Nasaq Challenge
//
//  Created by Daniel Tadrous on 12/22/18.
//  Copyright © 2018 Daniel Tadrous. All rights reserved.
//

import Foundation
import UIKit

extension UIColor{
   
    convenience init(hexString: String) {
        
        let hexString: String = (hexString as NSString).trimmingCharacters(in: .whitespacesAndNewlines)
        let scanner          = Scanner(string: hexString as String)
        
        if hexString.hasPrefix("#") {
            scanner.scanLocation = 1
        }
        var color: UInt32 = 0
        scanner.scanHexInt32(&color)
        
        let mask = 0x000000FF
        let r = Int(color >> 16) & mask
        let g = Int(color >> 8) & mask
        let b = Int(color) & mask
        
        let red   = CGFloat(r) / 255.0
        let green = CGFloat(g) / 255.0
        let blue  = CGFloat(b) / 255.0
        self.init(red:red, green:green, blue:blue, alpha:1)
    }
    static var sapphire: UIColor{
        get {
            return UIColor(red: 48 / 255.0, green: 35 / 255.0, blue: 174 / 255.0, alpha: 1)
        }
    }
    static var lavenderPink: UIColor{
        get {
            return UIColor(red: 200 / 255.0, green: 109 / 255.0, blue: 215 / 255.0, alpha: 1)
        }
    }
}
