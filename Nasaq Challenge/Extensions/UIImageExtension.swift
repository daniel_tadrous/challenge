//
//  UIImageExtension.swift
//  Nasaq Challenge
//
//  Created by Daniel Tadrous on 12/22/18.
//  Copyright © 2018 Daniel Tadrous. All rights reserved.
//

import Foundation
import UIKit

extension UIImage {
    
    static func createMainTabBarSelectionIndicator() -> UIImage {
        let roundView = UIView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        roundView.addGradientColor(start: UIColor.sapphire, end: UIColor.lavenderPink)
        roundView.layer.masksToBounds = true
        roundView.layer.cornerRadius = 25
        let roundViewC = UIView(frame: CGRect(x: 0, y: 0, width: 50, height: 60))
        roundViewC.addSubview(roundView)
        UIGraphicsBeginImageContext(roundViewC.bounds.size);
        roundViewC.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
}
