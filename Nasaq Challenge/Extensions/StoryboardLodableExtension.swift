//
//  StoryboardLodableExtension.swift
//  Nasaq Challenge
//
//  Created by Daniel Tadrous on 12/22/18.
//  Copyright © 2018 Daniel Tadrous. All rights reserved.
//

import Foundation
import UIKit

protocol PostsViewControllerStoryboardLoadable: StoryboardLodable {
}
protocol MainTabStoryboardLodable: StoryboardLodable {
}
protocol CameraStoryboardLoadable: StoryboardLodable {
}

extension PostsViewControllerStoryboardLoadable where Self: UIViewController {
    static var storyboardName: String {
        return "Posts"
    }
}

extension MainTabStoryboardLodable where Self: UIViewController {
    static var storyboardName: String {
        return "MainTab"
    }
}

extension CameraStoryboardLoadable where Self: UIViewController {
    static var storyboardName: String {
        return "Camera"
    }
}
