//
//  DateExtension.swift
//  Nasaq Challenge
//
//  Created by Daniel Tadrous on 12/22/18.
//  Copyright © 2018 Daniel Tadrous. All rights reserved.
//

import Foundation

extension Date{
    
    func getFormattedString(format: String) -> String {
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = format
        //todo change the next line in case there is more locale for the app
        dateFormatterPrint.locale = Locale(identifier: "en")
        return dateFormatterPrint.string(from: self)
    }
}
