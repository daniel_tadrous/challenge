//
//  UIViewExtension.swift
//  Nasaq Challenge
//
//  Created by Daniel Tadrous on 12/22/18.
//  Copyright © 2018 Daniel Tadrous. All rights reserved.
//

import Foundation
import UIKit

extension UIView{
    func addGradientColor(start: UIColor, end: UIColor) {
        let gl = CAGradientLayer()
        gl.colors = [start.cgColor, end.cgColor]
        gl.startPoint = CGPoint(x: 0.0, y: 0.5)
        gl.endPoint = CGPoint(x: 1.0, y: 0.5)
        gl.frame = self.bounds
        self.layer.insertSublayer(gl, at: 0)
    }
    func addRoundCorners(radious:CGFloat, corners: CACornerMask = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner]){
        if #available(iOS 11.0, *) {
            self.layer.cornerRadius = radious
            self.layer.maskedCorners = corners
        } else {
            // Fallback on earlier versions
            self.layer.cornerRadius = radious
        }
    }
}
