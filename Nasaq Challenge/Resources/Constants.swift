//
//  Constants.swift
//  Nasaq Challenge
//
//  Created by Daniel Tadrous on 12/22/18.
//  Copyright © 2018 Daniel Tadrous. All rights reserved.
//

import Foundation

let dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
let blackColorHex = "#0000000"
let defaultPlaceholderColor = "#C7C7CD"

//authrization token key
let authorizationKey = "Authorization"
//keyChain constants
let kSecClassValue = NSString(format: kSecClass)
let kSecAttrAccountValue = NSString(format: kSecAttrAccount)
let kSecValueDataValue = NSString(format: kSecValueData)
let kSecClassGenericTokenValue = NSString(format: kSecClassGenericPassword)
let kSecAttrServiceValue = NSString(format: kSecAttrService)
let kSecMatchLimitValue = NSString(format: kSecMatchLimit)
let kSecReturnDataValue = NSString(format: kSecReturnData)
let kSecMatchLimitOneValue = NSString(format: kSecMatchLimitOne)
let tokenKey = "token"
let service = "CodingChallenge"

