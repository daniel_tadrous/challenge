//
//  TextStrings.swift
//  Nasaq Challenge
//
//  Created by Daniel Tadrous on 12/22/18.
//  Copyright © 2018 Daniel Tadrous. All rights reserved.
//

import Foundation


enum L10n {
    
    // name for general table
    private static let t1 = "Localizable"
    
    static func getSeconds(num: Int)->String{
        if num == 1{
            return L10n.tr(t1, "second")
        }
        return L10n.tr(t1, "seconds", num)
    }
    static func getMinutes(num: Int)->String{
        if num == 1{
            return L10n.tr(t1, "minute")
        }
        return L10n.tr(t1, "minutes", num)
    }
    static func getHours(num: Int)->String{
        if num == 1{
            return L10n.tr(t1, "hour")
        }
        return L10n.tr(t1, "hours", num)
    }
    static func getDays(num: Int)->String{
        if num == 1{
            return L10n.tr(t1, "day")
        }
        return L10n.tr(t1, "days", num)
    }
    static func getStringFor(key: String) -> String {
        return L10n.tr(t1,key)
        
    }
    private static func tr(_ table: String, _ key: String, _ args: CVarArg...) -> String {
        let format = NSLocalizedString(key, tableName: table, bundle: Bundle(for: BundleToken.self), comment: "")
        return String(format: format, locale: Locale.current, arguments: args)
    }
}

private final class BundleToken {}
