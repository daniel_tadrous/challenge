//
//  Coordinator.swift
//  Nasaq Challenge
//
//  Created by Daniel Tadrous on 12/22/18.
//  Copyright © 2018 Daniel Tadrous. All rights reserved.
//

import Foundation
import UIKit
import Swinject
import SwinjectAutoregistration
import RxSwift

enum ScreensEnum:String{
    case PostsViewController, MainTabViewController, CameraViewController
}


/// A clas responsible about the whole app navigation
class Coordinator {
    private let window: UIWindow
    private let container: Container
    private let navigationController : UINavigationController
    static var shared: Coordinator!
    private var currentScreen: ScreensEnum!
    
    init(window: UIWindow, container: Container) {
        self.window = window
        self.container = container
        self.navigationController = UINavigationController()
        self.window.rootViewController = self.navigationController
      
        Coordinator.shared = self
    }
    
    /// it decides which screen should be opened at the first launch and load it
    func start(){
        _ = load(screenEnum: .MainTabViewController)
    }
    
    private func hideNavigationBar(_ hide:Bool = false){
        self.navigationController.navigationBar.isHidden = hide
        self.navigationController.setNavigationBarHidden(hide, animated: false)
    }
    
    func load(screenEnum: ScreensEnum, _ animated: Bool = true)->UIViewController{
        currentScreen = screenEnum
        var vc:UIViewController
        switch screenEnum {
            case .PostsViewController:
                vc = container.resolveViewController(PostsViewController.self)
                navigationController.pushViewController(vc, animated: animated)
            case .MainTabViewController :
                hideNavigationBar(true)
                vc = container.resolveViewController(MainTabViewController.self)
                navigationController.setViewControllers([vc], animated: animated)
        case .CameraViewController :
            hideNavigationBar(false)
            vc = container.resolveViewController(CameraViewController.self)
            navigationController.pushViewController(vc, animated: animated)
        }
        return vc
    }
   
}
