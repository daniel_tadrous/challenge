//
//  StoryboardLodable.swift
//  Nasaq Challenge
//
//  Created by Daniel Tadrous on 12/22/18.
//  Copyright © 2018 Daniel Tadrous. All rights reserved.
//

import Foundation
import UIKit

protocol StoryboardLodable {
    static var storyboardName: String { get }
}

