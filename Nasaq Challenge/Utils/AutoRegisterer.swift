//
//  AutoRegisterer.swift
//  Nasaq Challenge
//
//  Created by Daniel Tadrous on 12/22/18.
//  Copyright © 2018 Daniel Tadrous. All rights reserved.
//

import Foundation
import Swinject

class AutoRegisterer{
    
    class func register(container:Container) {
        
        // services
       
        container.autoregister(IPostsService.self, initializer: PostsService.init)
        container.autoregister(IProfilesService.self, initializer: ProfilesService.init)
       
        // viewModels
        container.autoregister(PostsViewModel.self, initializer: PostsViewModel.init)
        
        // viewControllers
        container.storyboardInitCompleted(PostsViewController.self) { r, c
            in c.viewModel = r.resolve(PostsViewModel.self)
        }
    }
}
