//
//  Post.swift
//  Nasaq Challenge
//
//  Created by Daniel Tadrous on 12/22/18.
//  Copyright © 2018 Daniel Tadrous. All rights reserved.
//

import Foundation
import ObjectMapper

struct PostsResponse : Mappable {
    
    var posts : [Post]?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        posts <- map["posts"]
    }
    
    struct Post : Mappable {
        var userName : String?
        var country : String?
        var profileImage : String?
        var postImage : String?
        private var date : Date?

        var readableTime: String {
            get{
                let interval = -1 * (date?.timeIntervalSinceNow ?? 0)
                let day: Int = Int(interval)/(60 * 60 * 24)
                if day > 0{
                    return L10n.getDays(num: day)
                }
                let hrs: Int = Int(interval)/(60 * 60)
                if hrs > 0{
                    return L10n.getHours(num: hrs)
                }
                let mins: Int = Int(interval)/(60)
                if mins > 0{
                    return L10n.getMinutes(num: mins)
                }
                return L10n.getSeconds(num: Int(interval))
            }
        }
        
        init?(map: Map) {
            self.date = (map.JSON["date"] as? String ?? "").getDate(format: dateFormat)
        }

        mutating func mapping(map: Map) {
            userName <- map["user_name"]
            country <- map["country"]
            profileImage <- map["profile_image"]
            postImage <- map["post_image"]
        }

    }
}
