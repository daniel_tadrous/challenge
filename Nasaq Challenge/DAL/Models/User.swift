//
//  User.swift
//  Nasaq Challenge
//
//  Created by Daniel Tadrous on 12/22/18.
//  Copyright © 2018 Daniel Tadrous. All rights reserved.
//

import Foundation
import ObjectMapper

struct UsersResponse: Mappable {
    
    var users: [User]?
    
    init?(map: Map) {
        
    }
    mutating func mapping(map: Map) {
        users <- map["users"]
    }
    
    struct User : Mappable {
        var userName : String?
        var profileImage : String?

        init?(map: Map) {

        }
        mutating func mapping(map: Map) {
            userName <- map["user_name"]
            profileImage <- map["profile_image"]
        }

    }
}
