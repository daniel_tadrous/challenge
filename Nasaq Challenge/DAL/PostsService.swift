//
//  PostsService.swift
//  Nasaq Challenge
//
//  Created by Daniel Tadrous on 12/22/18.
//  Copyright © 2018 Daniel Tadrous. All rights reserved.
//

import Foundation
import RxSwift
import AlamofireObjectMapper
import Alamofire

protocol IPostsService{
    /// get the posts list
    ///
    /// - Returns: observable of optional posts list
    func getPosts()->Observable<PostsResponse?>
}

class PostsService:BaseService, IPostsService{
    
    func getPosts() -> Observable<PostsResponse?> {
        let strUrl = "\(fullPathFor(type: .POSTS))"
        return getRequest(url: strUrl)
    }
}
