//
//  BaseService.swift
//  Nasaq Challenge
//
//  Created by Daniel Tadrous on 12/22/18.
//  Copyright © 2018 Daniel Tadrous. All rights reserved.
//

import Foundation
import RxSwift
import AlamofireObjectMapper
import Alamofire
import ObjectMapper

// add other end points here
enum EUrl: String{
    case POSTS = "data.php", PROFILES = "users.php"
}
class BaseService{
    
    // you can change base url in info.plist file
    private var baseUrl: String{
        get{
            return (Bundle.main.object(forInfoDictionaryKey: "base_url") as? String)!
        }
    }
    
    
    /// to get full url path
    ///
    /// - Parameter type: is the type of the endpoint used
    /// - Returns: full path
    func fullPathFor(type:EUrl) -> String {
        return "\(baseUrl)/\(type.rawValue)"
    }
    
    
    /// basic get request
    ///
    /// - Parameter url: full url for the endpoint
    /// - Returns: observable of generic object
    func getRequest<T>( url: String) -> Observable<T?> where T: Mappable{
        
        return Observable<T?>.create { (observer) -> Disposable in
            let requestReference = Alamofire.request(url, method: .get)
                .validate()
                .responseObject { (response: DataResponse<T>) in
                    switch response.result{
                    case .success:
                        observer.onNext(response.result.value)
                        observer.onCompleted()
                    case .failure:
                        observer.onError(response.error!)
                        observer.onCompleted()
                    }
            }
            return Disposables.create(with: {
                requestReference.cancel()
            })
        }
    }
}

