//
//  PostsService.swift
//  Nasaq Challenge
//
//  Created by Daniel Tadrous on 12/22/18.
//  Copyright © 2018 Daniel Tadrous. All rights reserved.
//

import Foundation
import RxSwift
import AlamofireObjectMapper
import Alamofire

protocol IProfilesService{
    /// get the profiles list
    ///
    /// - Returns: observable of optional profiles list
    func getProfiles()->Observable<UsersResponse?>
}

class ProfilesService:BaseService, IProfilesService{
    
    func getProfiles() -> Observable<UsersResponse?> {
        let strUrl = "\(fullPathFor(type: .PROFILES))"
        return getRequest(url: strUrl)
    }
}
