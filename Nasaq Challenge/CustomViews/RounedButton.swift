//
//  RounedButton.swift
//  Nasaq Challenge
//
//  Created by Daniel Tadrous on 12/22/18.
//  Copyright © 2018 Daniel Tadrous. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable class RoundedButton: UIButton {
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = bounds.height / 2
        clipsToBounds = true
    }
}
