//
//  RoundedView.swift
//  Nasaq Challenge
//
//  Created by Daniel Tadrous on 12/22/18.
//  Copyright © 2018 Daniel Tadrous. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable class RoundedView: UIView{
    
    @IBInspectable
    var Radius: CGFloat = 0{
        didSet{
            self.addRoundCorners(radious: self.Radius, corners: [.layerMinXMinYCorner, .layerMinXMaxYCorner, .layerMaxXMinYCorner, .layerMaxXMaxYCorner])
        }
    }
    
    @IBInspectable
    var HasBorder: Bool = false{
        didSet{
            if self.HasBorder{
                self.layer.borderWidth = 1
            }
        }
    }
    @IBInspectable
    var BorderColor: UIColor = UIColor.clear{
        didSet{
            if self.HasBorder{
                self.layer.borderColor = self.BorderColor.cgColor
            }
        }
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    
    }
    
}
