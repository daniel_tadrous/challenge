//
//  MainTabViewController.swift
//  Nasaq Challenge
//
//  Created by Daniel Tadrous on 12/22/18.
//  Copyright © 2018 Daniel Tadrous. All rights reserved.
//

import UIKit



class MainTabViewController:UITabBarController, MainTabStoryboardLodable{
    
    let kBarHeight:CGFloat = 70;
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tabBar.tintColor = UIColor.white
        tabBar.unselectedItemTintColor = UIColor.black
        tabBar.selectionIndicatorImage = UIImage.createMainTabBarSelectionIndicator()
        self.selectedIndex = 2
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        var tabFrame = self.tabBar.frame
        tabFrame.size.height = kBarHeight
        if #available(iOS 11.0, *) {
            tabFrame.size.height = kBarHeight + (UIApplication.shared.keyWindow?.safeAreaInsets.bottom)!
            tabFrame.origin.y = self.view.frame.height - kBarHeight - (UIApplication.shared.keyWindow?.safeAreaInsets.bottom)!
        } else {
            tabFrame.origin.y = self.view.frame.height - kBarHeight
        };
        self.tabBar.frame = tabFrame

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
}


