//
//  PostsViewController.swift
//  Nasaq Challenge
//
//  Created by Daniel Tadrous on 12/22/18.
//  Copyright © 2018 Daniel Tadrous. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import SDWebImage
import SimpleImageViewer

class PostsViewController: UIViewController, PostsViewControllerStoryboardLoadable {

    @IBOutlet weak var tableView: UITableView!
    
    var viewModel: PostsViewModel!
    let headerCellID = "HeaderCell"
    let postCellID = "PostCell"
    let profilesCellID = "ProfilesCell"
    let headersHeight = CGFloat(40.0)
    private let disposeBag = DisposeBag()
    
    var posts = [PostsResponse.Post]()
    var profiles = [UsersResponse.User]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.register(UINib(nibName: headerCellID, bundle: nil), forCellReuseIdentifier: headerCellID)
        self.tableView.register(UINib(nibName: postCellID, bundle: nil), forCellReuseIdentifier: postCellID)
        self.tableView.register(UINib(nibName: profilesCellID, bundle: nil), forCellReuseIdentifier: profilesCellID)
        self.tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: self.tableView.frame.width, height: 20))
        Observable.combineLatest(
            self.viewModel.posts.asObservable(), self.viewModel.profiles.asObservable(),
            resultSelector: { value1, value2 in
                if value1 != nil && value2 != nil{
                    self.posts = value1!
                    self.profiles = value2!
                    self.tableView.reloadData()
                }
                
        }).observeOn(MainScheduler.instance)
            .subscribe()
            .disposed(by: disposeBag)
    }
    
    private func openImage(imageView: UIImageView){
        let configuration = ImageViewerConfiguration { config in
            config.imageView = imageView
        }
        
        let imageViewerController = ImageViewerController(configuration: configuration)
        
        present(imageViewerController, animated: true)
    }
    
    
}
extension PostsViewController: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0, 1:
            return self.profiles.count > 0 ? 1 : 0
        default:
            return self.posts.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: headerCellID, for: indexPath) as! HeaderCell
            cell.cameraClick = {
                let vc = Coordinator.shared.load(screenEnum: .CameraViewController)
                (vc as! CameraViewController).delegate = self
            }
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: profilesCellID, for: indexPath) as! ProfilesCell
            cell.setData(profiles: self.profiles)
            return cell
        case 2:
            let post = self.posts[indexPath.row]
            let cell = tableView.dequeueReusableCell(withIdentifier: postCellID, for: indexPath) as! PostCell
            cell.setData(post: post)
            cell.onClick = { imageView in
                
                self.openImage(imageView: imageView)
            }
            return cell
        default:
            return UITableViewCell()
        }
    }
}

extension PostsViewController: ImagePickerDelegate{
    func imagePicked(image: UIImage?) {
        //TODO use the image to do something.
    }
}
