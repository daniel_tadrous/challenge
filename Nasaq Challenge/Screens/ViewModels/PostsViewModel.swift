//
//  PostsViewModel.swift
//  Nasaq Challenge
//
//  Created by Daniel Tadrous on 12/22/18.
//  Copyright © 2018 Daniel Tadrous. All rights reserved.
//

import Foundation
import RxSwift

class PostsViewModel{
    
    private var postsService: IPostsService
    private var profilesService: IProfilesService
    private let disposeBag = DisposeBag()
    
    let posts = Variable<[PostsResponse.Post]?>(nil)
    let profiles = Variable<[UsersResponse.User]?>(nil)
    
    init(postsService: IPostsService, profilesService: IProfilesService) {
        self.postsService = postsService
        self.profilesService = profilesService
        self.getPosts()
        self.getProfiles()
    }
    
    private func getPosts(){
        self.postsService.getPosts().subscribe(onNext: { [unowned self](postsResponse) in
            if let postsResponse = postsResponse{
                self.posts.value = postsResponse.posts
            }
        }).disposed(by: disposeBag)
    }
    private func getProfiles(){
        self.profilesService.getProfiles().subscribe(onNext: { [unowned self](profilesResponse) in
            if let profilesResponse = profilesResponse{
                self.profiles.value = profilesResponse.users
            }
        }).disposed(by: disposeBag)
    }
}
