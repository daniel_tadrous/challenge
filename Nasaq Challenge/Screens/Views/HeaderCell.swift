//
//  HeaderCell.swift
//  Nasaq Challenge
//
//  Created by Daniel Tadrous on 12/22/18.
//  Copyright © 2018 Daniel Tadrous. All rights reserved.
//

import UIKit

class HeaderCell: UITableViewCell {
    
    var cameraClick: (()->Void)?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        cameraViewHolder.addGradientColor(start: UIColor.sapphire, end: UIColor.lavenderPink)
        cameraViewHolder.addRoundCorners(radious: 16)
    }
    
    @IBOutlet weak var cameraViewHolder: UIView!
 
    @IBAction func cameraClickHandler(_ sender: UIButton) {
        cameraClick?()
    }
}
