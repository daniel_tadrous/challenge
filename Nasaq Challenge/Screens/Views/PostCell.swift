//
//  PostCell.swift
//  Nasaq Challenge
//
//  Created by Daniel Tadrous on 12/22/18.
//  Copyright © 2018 Daniel Tadrous. All rights reserved.
//

import UIKit
import SDWebImage


class PostCell: UITableViewCell {

    @IBOutlet weak var imageContainerView: RoundedView!
    
    var onClick: ((UIImageView)-> Void)?
    @IBOutlet weak var profileImageView: UIImageView!
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var countryLabel: UILabel!
    
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var postImageView: UIImageView!
    
    @IBAction func imageClickHandler(_ sender: UIButton) {
        self.onClick?(self.postImageView)
    }
    
    func setData(post: PostsResponse.Post){
        self.imageContainerView.addGradientColor(start: UIColor.sapphire, end: UIColor.lavenderPink)
        self.profileImageView.sd_setImage(with: URL(string: post.profileImage!))
        self.profileImageView.layer.borderColor = UIColor.white.cgColor
        self.profileImageView.layer.borderWidth = 1
        self.postImageView.sd_setImage(with: URL(string: post.postImage!))
        self.nameLabel.text = post.userName
        self.countryLabel.text = post.country
        self.dateLabel.text = post.readableTime        
    }
}
