//
//  ProfilesCell.swift
//  Nasaq Challenge
//
//  Created by Daniel Tadrous on 12/22/18.
//  Copyright © 2018 daniel. All rights reserved.
//

import UIKit

class ProfilesCell: UITableViewCell {

    @IBOutlet weak var collectionView: UICollectionView!
    private var profiles = [UsersResponse.User]()
    let profileCellID = "ProfileCollectionViewCell"
    let collectionCellWidth = 55
    func setData(profiles: [UsersResponse.User]){
        self.collectionView.register(UINib(nibName: profileCellID, bundle: nil), forCellWithReuseIdentifier: profileCellID)
        self.profiles = profiles
    }
}
extension ProfilesCell: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.profiles.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let profile = profiles[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: profileCellID, for: indexPath) as! ProfileCollectionViewCell
        cell.setData(profile: profile)
        return cell
    }
    
  
}
