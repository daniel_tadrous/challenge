//
//  ProfileCollectionViewCell.swift
//  Nasaq Challenge
//
//  Created by Daniel Tadrous on 12/22/18.
//  Copyright © 2018 daniel. All rights reserved.
//

import UIKit
import SDWebImage

class ProfileCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var titleLbl: UILabel!
    
    @IBOutlet weak var profileImageView: UIImageView!
    
    @IBOutlet weak var imageViewContainer: RoundedView!
    
    func setData(profile: UsersResponse.User){
        self.titleLbl.text = profile.userName
        self.imageViewContainer.addGradientColor(start: UIColor.sapphire, end: UIColor.lavenderPink)
        self.profileImageView.sd_setImage(with: URL(string: profile.profileImage ?? ""))
        self.profileImageView.layer.borderColor = UIColor.white.cgColor
        self.profileImageView.layer.borderWidth = 1
    }
}
